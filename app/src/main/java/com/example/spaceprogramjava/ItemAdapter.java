package com.example.spaceprogramjava;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.spaceprogramjava.model.Item;

import java.util.List;

public class ItemAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    List<Item> items;

    ItemAdapter(Context context, List<Item> items) {
        ctx = context;
        this.items = items;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        @SuppressLint("ViewHolder") View view = lInflater.inflate(R.layout.item, parent, false);

        Item p =(Item) getItem(position);

        ((TextView) view.findViewById(R.id.name)).setText(p.getName());
        ((TextView) view.findViewById(R.id.weight)).setText(p.getWeight().toString() + "kg");
        Button button = view.findViewById(R.id.delete_button);
        button.setTag(position);
        button.setOnClickListener(view1 -> {
            int index = (int) view1.getTag();
            items.remove(index);
            notifyDataSetChanged();
        });



        return view;
    }
}