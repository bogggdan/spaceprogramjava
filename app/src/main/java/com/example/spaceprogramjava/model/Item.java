package com.example.spaceprogramjava.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Item implements Parcelable {
    private final String name;
    private final Integer weight;

    public Item(String name, Integer weight) {
        this.name = name;
        this.weight = weight;
    }

    protected Item(Parcel in) {
        name = in.readString();
        if (in.readByte() == 0) {
            weight = null;
        } else {
            weight = in.readInt();
        }
    }

    public Integer getWeight() {
        return weight;
    }

    public String getName() {
        return name;
    }

    public static final Creator<Item> CREATOR = new Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        if (weight == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(weight);
        }
    }
}

