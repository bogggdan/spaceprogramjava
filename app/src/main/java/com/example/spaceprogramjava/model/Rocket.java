package com.example.spaceprogramjava.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Rocket implements SpaceShip, Parcelable {
    private final Integer cost;
    private final Integer weight;
    private final Integer maxWeight;
    private final List<Item> cargo;

    public Rocket(Integer cost, Integer weight, Integer maxWeight) {
        this.cost = cost;
        this.weight = weight;
        this.maxWeight = maxWeight;
        this.cargo = new ArrayList<>();
    }

    protected Rocket(Parcel in) {
        if (in.readByte() == 0) {
            cost = null;
        } else {
            cost = in.readInt();
        }
        if (in.readByte() == 0) {
            weight = null;
        } else {
            weight = in.readInt();
        }
        if (in.readByte() == 0) {
            maxWeight = null;
        } else {
            maxWeight = in.readInt();
        }
        cargo = in.createTypedArrayList(Item.CREATOR);
    }

    @Override
    public boolean launch() {
        return true;
    }

    @Override
    public boolean land() {
        return true;
    }

    @Override
    public boolean canCarry(Item item) {

        return getWeightLeft() - item.getWeight() >= 0;
    }

    @Override
    public void carry(Item item) {
        cargo.add(item);
    }

    public Integer getCost() {
        return cost;
    }

    public double countCargoWeight() {
        double carriedWeight = 0;

        for (Item carriedItem : cargo) {
            carriedWeight += carriedItem.getWeight();
        }

        return carriedWeight;
    }

    public double getWeightLeft() {
        return maxWeight - weight - countCargoWeight();
    }

    public Integer getWeight() {
        return weight;
    }

    public Integer getMaxWeight() {
        return maxWeight;
    }

    public List<Item> getCargo() {
        return cargo;
    }

    public static final Creator<Rocket> CREATOR = new Creator<Rocket>() {
        @Override
        public Rocket createFromParcel(Parcel in) {
            return new Rocket(in);
        }

        @Override
        public Rocket[] newArray(int size) {
            return new Rocket[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (cost == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(cost);
        }
        if (weight == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(weight);
        }
        if (maxWeight == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(maxWeight);
        }
        parcel.writeTypedList(cargo);
    }
}
