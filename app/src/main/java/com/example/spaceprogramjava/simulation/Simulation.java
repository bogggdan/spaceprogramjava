package com.example.spaceprogramjava.simulation;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.spaceprogramjava.model.Item;
import com.example.spaceprogramjava.model.Rocket;
import com.example.spaceprogramjava.spaceship.U1;
import com.example.spaceprogramjava.spaceship.U2;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Simulation {
    @RequiresApi(api= Build.VERSION_CODES.N)
    public ArrayList<U1> loadU1(List<Item> items) {
        items.sort((item, t1) -> item.getWeight().compareTo(t1.getWeight()) * -1);
        ArrayList<U1> spaceships = new ArrayList<>();
        LinkedList<Item> cargo = new LinkedList<>(items);

        while (!cargo.isEmpty()) {
            U1 rocket = new U1();

            if (!rocket.canCarry(cargo.getLast())) {
                break;
            }

            for (Item item : cargo) {
                if (rocket.canCarry(item)) {
                    rocket.carry(item);
                }
            }

            cargo.removeAll(rocket.getCargo());
            spaceships.add(rocket);
        }

        return spaceships;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public ArrayList<U2> loadU2(List<Item> items) {
        items.sort((item, t1) -> item.getWeight().compareTo(t1.getWeight()) * -1);
        ArrayList<U2> spaceships = new ArrayList<>();
        LinkedList<Item> cargo = new LinkedList<>(items);

        while (!cargo.isEmpty()) {
            U2 rocket = new U2();

            if (rocket.getWeightLeft() < cargo.getLast().getWeight()) {
                break;
            }

            for (Item item : cargo) {
                if (rocket.canCarry(item)) {
                    rocket.carry(item);
                }
            }

            cargo.removeAll(rocket.getCargo());
            spaceships.add(rocket);
        }

        return spaceships;
    }

    public Integer runSimulation(List<? extends Rocket> spaceships) {
        int budget = 0;

        for (Rocket rocket : spaceships) {
            do {
                budget += rocket.getCost();
            } while (!rocket.launch() || !rocket.land());
        }

        return budget;
    }
}
