package com.example.spaceprogramjava.spaceship;

import com.example.spaceprogramjava.model.Rocket;

public class U1 extends Rocket {
    public U1() {
        super(100, 10000, 18000);
    }

    @Override
    public boolean launch() {
        return Math.random() * 100 > (5 * (countCargoWeight() / (getMaxWeight() - getWeight())));
    }

    @Override
    public boolean land() {
        return Math.random() * 100 > (countCargoWeight() / (getMaxWeight() - getWeight()));
    }
}
