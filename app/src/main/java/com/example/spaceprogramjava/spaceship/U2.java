package com.example.spaceprogramjava.spaceship;

import com.example.spaceprogramjava.model.Rocket;

public class U2 extends Rocket {
    public U2() {
        super(120, 18000, 29000);
    }

    @Override
    public boolean launch() {
        return Math.random() * 100 > (4 * (countCargoWeight() / (getMaxWeight() - getWeight())));
    }

    @Override
    public boolean land() {
        return Math.random() * 100 > (8 * (countCargoWeight() / (getMaxWeight() - getWeight())));
    }
}
