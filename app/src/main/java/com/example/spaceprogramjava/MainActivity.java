package com.example.spaceprogramjava;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.spaceprogramjava.model.Item;
import com.example.spaceprogramjava.simulation.Simulation;
import com.example.spaceprogramjava.spaceship.U1;
import com.example.spaceprogramjava.spaceship.U2;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ArrayList<Item> items = new ArrayList<>();
    private final Simulation simulation = new Simulation();
    private ArrayList<U1> spaceshipsU1 = new ArrayList<>();
    private ArrayList<U2> spaceshipsU2 = new ArrayList<>();
    private ItemAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            items = savedInstanceState.getParcelableArrayList("items");
            spaceshipsU1 = savedInstanceState.getParcelableArrayList("u1");
            spaceshipsU2 = savedInstanceState.getParcelableArrayList("u2");
        }

        setContentView(R.layout.activity_main);
        ListView listView = findViewById(R.id.items);
        adapter = new ItemAdapter(this, items);
        listView.setAdapter(adapter);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelableArrayList("items", items);
        outState.putParcelableArrayList("u1", spaceshipsU1);
        outState.putParcelableArrayList("u2", spaceshipsU2);
        super.onSaveInstanceState(outState);
    }

    public void addClick(View view) {
        TextView name = findViewById(R.id.item_name);
        TextView weight = findViewById(R.id.item_weight);

        if (name.getText().toString().isEmpty() || weight.getText().toString().isEmpty()) {
            Toast.makeText(this, this.getText(R.string.empty_toast), Toast.LENGTH_LONG).show();
        } else {
            items.add(new Item(name.getText().toString(), Integer.decode(weight.getText().toString())));
            adapter.notifyDataSetChanged();
        }

        name.getEditableText().clear();
        weight.getEditableText().clear();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void fillU1Click(View view) {
        spaceshipsU1 = simulation.loadU1(items);
    }

    public void clearClick(View view) {
        items.clear();
        spaceshipsU2.clear();
        spaceshipsU1.clear();
        adapter.notifyDataSetChanged();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void fillU2Click(View view) {
        spaceshipsU2 = simulation.loadU2(items);
    }

    public void startClick(View view) {
        if (spaceshipsU1.isEmpty() && spaceshipsU2.isEmpty()) {
            Toast.makeText(this, this.getText(R.string.rockets_toast), Toast.LENGTH_LONG).show();
            return;
        }

        int totalU1 = simulation.runSimulation(spaceshipsU1);
        int totalU2 = simulation.runSimulation(spaceshipsU2);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Space program cost")
                .setMessage("For u1 rockets: " + totalU1 + "million$\n"
                        + "For u2 rockets: " + totalU2 + "million$\n")
                .setPositiveButton("OK", (dialog, id) -> dialog.cancel());
        builder.show();
    }
}